
在配置文件中，提到ERPC内部使用的是开源的zlog日志库，并且提供了通用的日志接口，以便模块和应用开发者快速使用。下面就简单介绍ERPC封装后的日志接口。

ERPC对外提供的日志接口头文件为rpc_logger.h，非常简单，全部内容如下：
```
#ifndef __RPC_LOGGER__H__
#define __RPC_LOGGER_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "zlog.h"

#define log_init(path, cat)     dzlog_init(path, cat)
#define log_reload(path)        zlog_reload(path)
#define log_deinit()            zlog_fini()

#define LOG_F    dzlog_fatal
#define LOG_E    dzlog_error
#define LOG_W    dzlog_warn
#define LOG_N    dzlog_notice
#define LOG_I    dzlog_info
#define LOG_D    dzlog_debug

#ifdef LOG_E
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_ERROR(...)             LOG_E(__VA_ARGS__)
    #else
        #define LOG_ERROR(format, args...) LOG_E(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_ERROR(...)             printf(__VA_ARGS__)
    #else
        #define LOG_ERROR(format, args...) printf("[ERROR]  [%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_W
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_WARN(...)              LOG_W(__VA_ARGS__)
    #else
        #define LOG_WARN(format, args...)  LOG_W(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_WARN(...)              printf(__VA_ARGS__)
    #else
        #define LOG_WARN(format, args...)  printf("[WARNING][%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_N
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_NOTICE(...)              LOG_N(__VA_ARGS__)
    #else
        #define LOG_NOTICE(format, args...)  LOG_N(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_NOTICE(...)              printf(__VA_ARGS__)
    #else
        #define LOG_NOTICE(format, args...)  printf("[NOTICE] [%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_I
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_INFO(...)              LOG_I(__VA_ARGS__)
    #else
        #define LOG_INFO(format, args...)  LOG_I(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_INFO(...)              printf(__VA_ARGS__)
    #else
        #define LOG_INFO(format, args...)  printf("[INFO]   [%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_D
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_DEBUG(...)             LOG_D(__VA_ARGS__)
    #else
        #define LOG_DEBUG(format, args...) LOG_D(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_DEBUG(...)
    #else
        #define LOG_DEBUG(format, args...)
    #endif
#endif

#ifdef __cplusplus
}
#endif

#endif // __RPC_LOGGER_H__
```

由于ERPC内部也使用的zlog，所有模块和应用开发者不用关心和调用log_init、log_reload、logdeinit三个接口。只需要关注如下接口即可：
```
LOG_ERROR()    // 错误日志
LOG_WARN()     // 警告日志
LOG_NOTICE()   // 提示日志
LOG_INFO()     // 信息日志
LOG_DEBUG()    // 调试日志
```

接口参数规则全部遵循printf()方法。祝使用愉快o(∩_∩)o 。
